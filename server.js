var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var flash = require("flash");
var passport = require("passport");
LocalStrategy = require('passport-local').Strategy;

var Sequelize = require('sequelize');
var pg = require('pg');
var PassportLocalStrategy = require('passport-local').Strategy;

var sequelize = new Sequelize('postgres://localhost:5432/ticket');

var User = sequelize.define('user', {
    username: Sequelize.STRING,
    password: Sequelize.STRING,
});
User.sync();



// 'use strict';
// var crypto = require('crypto');
//
// module.exports = function(sequelize, DataTypes) {
//     var User = sequelize.define('User', {
//         username: DataTypes.STRING,
//         first_name: DataTypes.STRING,
//         last_name: DataTypes.STRING,
//         salt: DataTypes.STRING,
//         hashed_pwd: DataTypes.STRING
//     }, {
//         classMethods: {
//
//         },
//         instanceMethods: {
//             createSalt: function() {
//                 return crypto.randomBytes(128).toString('base64');
//             },
//             hashPassword: function(salt, pwd) {
//                 var hmac = crypto.createHmac('sha1', salt);
//
//                 return hmac.update(pwd).digest('hex');
//             },
//             authenticate: function(passwordToMatch) {
//                 return this.hashPassword(this.salt, passwordToMatch) === this.hashed_pwd;
//             }
//         }
//     });
//     return User;
// };

var auth = {};
auth.localStrategy = new PassportLocalStrategy({
        username: 'username',
        password: 'password'
    },

    function (username, password, done){
        var User = require('./User').User;
        User.find({username: username}).success(function(user){
            if (!user){
                return done(null, false, { message: 'Nobody here by that name'} );
            }
            if (user.password !== password){
                return done(null, false, { message: 'Wrong password'} );
            }
            return done(null, { username: user.username });
        });
    }
);

auth.validPassword = function(password){
    return this.password === password;
}

auth.serializeUser = function(user, done){
    done(null, user);
};

auth.deserializeUser = function(obj, done){
    done(null, obj);
};

var passport = require('passport');
var AuthController = {

    login: passport.authenticate('local', {
        successRedirect: '/auth/login/success',
        failureRedirect: '/auth/login/failure'
    }),

    loginSuccess: function(req, res){
        res.json({
            success: true,
            user: req.session.passport.user
        });
    },

    loginFailure: function(req, res){
        res.json({
            success:false,
            message: 'Invalid username or password.'
        });
    },

    logout: function(req, res){
        req.logout();
        res.end();
    },
};

var app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());
app.use(session({secret: 'secret'}))
app.use(passport.initialize());
app.use(passport.session());
// app.use(app.router);





module.exports = app;
