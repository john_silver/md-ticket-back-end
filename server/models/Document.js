var mongoose = require('mongoose');


// document schema
var documentSchema = new mongoose.Schema({
    // regular

    createDate: { type: Date, default: Date.now },
    docType : String,
    number : Number, // Порядковый номер документа
    body : String,

    contragent : { type: mongoose.Schema.ObjectId, ref: 'Contragent'},
    user : { type: mongoose.Schema.ObjectId, ref: 'User' }

});


module.exports = mongoose.model('Document', documentSchema);