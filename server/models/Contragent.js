var mongoose = require('mongoose');


// user schema
var contragentSchema = new mongoose.Schema({
    // regular
    country: String,
    companyName: String,
    urAdress: String,
    bin : String,
    nameBank : String,
    accountNumber : String,
    postIndex : String,
    prefix : String,
    user : { type: mongoose.Schema.ObjectId, ref: 'User' }

});


module.exports = mongoose.model('Contragent', contragentSchema);