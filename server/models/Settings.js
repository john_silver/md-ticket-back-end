var mongoose = require('mongoose');


// user schema
var settingsSchema = new mongoose.Schema({
    // regular
    copyToEmail : Boolean,
    sendSMSAfterFihish: Boolean,
    user : { type: mongoose.Schema.ObjectId, ref: 'User' }
});


module.exports = mongoose.model('Settings', settingsSchema);