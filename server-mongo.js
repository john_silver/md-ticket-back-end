var compression = require('compression');
var express = require('express');

var path = require('path');
var logger = require('morgan');
var http = require('http');
var cors = require('cors');

var mongoose = require('mongoose');
require('mongoose-double')(mongoose);

/*var fs = require('fs');*/

var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var bcrypt = require('bcryptjs');
var crypto = require('crypto');
var async = require('async');

var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var nodemailer = require('nodemailer');




// Passport local
var SchemaTypes = mongoose.Schema.Types;
passport.use(new LocalStrategy({ usernameField: 'idToEnter' }, function( idToEnter, password, done) {
    User.findOne({ idToEnter: idToEnter },'name password idToEnter', function(err, user) {
        if (err) return done(err);
        if (!user) return done(null, false);
        user.comparePassword(password, function(err, isMatch) {
            if (err) return done(err);
            if (isMatch) return done(null, user);
            return done(null, false);
        });
    });
}));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});


var User = require('./server/models/User');
var Contragent = require('./server/models/Contragent');
var Document = require('./server/models/Document');
var Settings = require('./server/models/Settings');


mongoose.connect('mongodb://127.0.0.1:27017/ticket');

var app = express();

// Middlewares
app.use(cors());
app.set('port', process.env.PORT || 8080);
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(session({ secret: 'some key',
    resave:  true,
    saveUninitialized: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection })
}));
app.use(passport.initialize());
app.use(passport.session());


app.use(express.static(path.join(__dirname, 'public'), { maxAge: 1 }));

// Access


function unloggedIn(req, res, next) {
    if (!req.user) {
        res.redirect('/#newinvoice');
    } else {
        next();
    }
}

// app.use(require('./server/routes'));


// unlogged

app.use('/client', unloggedIn);

app.use('/info', unloggedIn);

app.use('/profile', unloggedIn);

app.use('/invoices', unloggedIn);

// Use of Passport user after login
app.use(function(req, res, next) {
    if (req.user) {
        res.cookie('user', JSON.stringify(req.user));
    }
    next();
});


//
// User.find().exec(function(err, result){
//     if (err) return next(err);
//     console.log(result);
//     result.forEach(function (item) {
//         item.password = item.idToEnter;
//         console.log(item);
//         item.save(function(err) {
//             if (err) return next(err);
//             console.log('save');
//
//         });
//     });
//
// });


app.get('/', function (req, res, next) {
    res.status(200).send({'message': 'get /'});
});


// Authentication
app.post('/api/signup', function(req, res, next) {
    async.waterfall([
        function( done) {
            var user = new User({
                // regular
                idToEnter: req.body.idToEnter,
                name: req.body.name,
                password: req.body.password

            });
            user.save(function(err) {
                if (err) return next(err);
                req.login(user, function(err) {
                    if (err) return next(err);
                    return res.json(user);
                });
                done(err, 'done');

            });
        }
    ])



});

app.post('/api/login', passport.authenticate('local'), function(req, res) {
    console.log(req.user);
    res.cookie('user', JSON.stringify(req.user));
    res.send(req.user);
});

app.get('/api/logout', function(req, res, next) {
    req.logout();
    res.send(200);
});

app.post('/api/changePassword', function (req, res, next) {
    // userId, password: this.password, newPassword: this.newPassword
    // UserModel.findById(id, function (err, user) { ... } );
    User.findById( req.body.userId , function(err, user) {
        if (err) return done(err);
        if (!user) return done(null, false);
        user.comparePassword(req.body.password, function(err, isMatch) {
            if (err) return done(err);
            if (isMatch) {
                user.password = req.body.newPassword;
                user.save(function (err) {
                    res.status(200).send({user: user, message: 'Password update'});
                });
            }else{
                res.status(400).send({user: user, message: 'Password !isMatch'});
            }
        });
    });

});

app.get('/api/documents', function (req, res, next) {
    console.log('req.user');
    console.log(req.user);
    Document.find({user: req.query.userId}, 'createDate docType number contragent user').populate('contragent', 'companyName').populate('user', 'representative').exec(function(err, result){
        if (err) return next(err);
        var contragent = result;
        res.status(200).send({ document: result, message: 'Success'});
    });
});


app.post('/api/documents', function (req, res, next) {
    async.waterfall([
        function(done) {


            console.log(req.body.userId);
            if (req.body.document.contragent==undefined){
                done(null, null, false);
            }else {
                Contragent.findOne({
                    companyName: req.body.document.contragent.companyName,
                    user: req.body.userId
                }).exec(function (err, result) {
                    if (err) return next(err);
                    console.log(result);
                    var contragent = result;
                    done(err, contragent, true);
                });
            }

        },
        function(contragent, create,done) {
            if (create) {
                if (contragent == null) {
                    console.log('создаем нового контрагента');
                    var contragent = new Contragent({
                        accountNumber: req.body.document.contragent.accountNumber,
                        country: req.body.document.contragent.country,
                        companyName: req.body.document.contragent.companyName,
                        urAdress: req.body.document.contragent.urAdress,
                        bin: req.body.document.contragent.bin,
                        nameBank: req.body.document.contragent.nameBank,
                        postIndex: req.body.document.contragent.postIndex,
                        prefix: req.body.document.contragent.prefix,


                        user: req.body.userId
                    });
                    contragent.save(function (err) {
                        done(err, contragent, "Contraget created");
                    });
                } else {
                    console.log('Обновляем контрагента');
                    contragent.accountNumber = req.body.document.contragent.accountNumber;
                    contragent.country = req.body.document.contragent.country;
                    contragent.companyName = req.body.document.contragent.companyName;
                    contragent.urAdress = req.body.document.contragent.urAdress;
                    contragent.bin = req.body.document.contragent.bin;
                    contragent.nameBank = req.body.document.contragent.nameBank;
                    contragent.postIndex = req.body.document.contragent.postIndex;
                    contragent.prefix = req.body.document.contragent.prefix;
                    contragent.save(function (err) {
                        done(err, contragent, "Contraget update");
                    });
                }
            }else{
                done(null , null, "Contraget not send");
            }

        },
        function (contragent, message, done) {
            User.findById( req.body.userId , function(err, user) {
                done(err, contragent, user, "Contraget update");


            });
        },
        function(contragent, user, message, done) {
            console.log(user);
            var doc =  req.body.document;
            var docType = doc.docType;
            delete  doc.docType;
            var document = new Document({
                // createDate: { type: Date, default: Date.now }, само создастся
                docType : docType,
                number : req.body.document.number, // Порядковый номер документа
                body : JSON.stringify(req.body.document),

                contragent : contragent,
                user : req.body.userId
            });

            document.save(function(err){

                var html = '';
                var attach = [];
                html += '<br/>Создан: '+document.createDate;
                html += '<br/>Документ: '+document.docType;
                html += '<br/>Номер: '+ document.number;
                if (contragent){
                    html += '<br/>Страна: '+contragent.country;
                    html += '<br/>Контрагент: '+contragent.prefix+' '+ contragent.companyName;
                    html += '<br/>БИН: '+contragent.bin;
                    html += '<br/>Банк: '+contragent.nameBank;
                    html += '<br/>Номер расчетного счета: '+contragent.accountNumber;
                    html += '<br/>Индекс: '+contragent.postIndex;
                }
                var doc = req.body.document;

                if (doc.sendCopyToContragent) {
                    html += '<br/>Копию отправить контрагенту';
                    html += '<br/>Почта: ' + doc.emailContragent;
                    html += '<br/>Имя контрагента: ' + doc.nameContragent;
                }
                if (doc.sendOriginalTOContragent) {
                    html += '<br/>Отправить оригинал по почте контрагенту';

                    html += '<br/>Доставить контрагенту: ' + doc.sendContragent;
                    html += '<br/>Забрать у контрагента: ' + doc.reciveToContragent;
                    html += '<br/>Поручить контроль MD: ' + doc.entrustToMD;
                    html += '<br/>Ответственное лицо: ' + doc.clientName;
                    html += '<br/>Контактный телефон: ' + doc.contactPhone;
                }

                html += '<br/>Требуется срочно: '+ doc.srochno;
                // sendOriginalTOContragent

                if (doc.dopSformirovat){
                    html += '<br/>Дополнительно сформировать накладную '+ doc.dopSformirovat.nakladnaya;
                    html += '<br/>Дополнительно сформировать приходник '+ doc.dopSformirovat.prihodnik;

                }

                if (document.docType == 'Счет-фактура'){
                    var items = req.body.document.items;
                    items.forEach(function (item) {
                        html += '<br/>Номер:  '+item.index ;
                        html += '<br/>Единица измерения: '+item.unit ;
                        html += '<br/>Валюта: '+item.currency ;
                        html += '<br/>Код: '+item.code ;
                        html += '<br/>Наименование: '+item.name ;
                        html += '<br/>Количество: '+item.count ;
                        html += '<br/>Сумма: '+item.summ ;
                        html += '<br/>Цена: '+item.price ;
                    });
                    html += '<br/>naOsnovanii '+req.body.document.naOsnovanii;
                    if (req.body.document.naOsnovanii=='Договора'){
                        html += '<br/>osnovanie.nomerDogovora'+ doc.osnovanie.nomerDogovora;
                        html += '<br/>osnovanie.date'+ doc.osnovanie.date;
                    }
                    if (req.body.document.naOsnovanii=="Счет на оплату"){
                        var file = {
                                filename: doc.schetNaOplatu.filename,
                                content: doc.schetNaOplatu.base64,
                                encoding: 'base64'
                            };
                        attach.push(file);
                    }
                    if (req.body.document.naOsnovanii=="Доверенности"){
                        var file = {
                            filename: doc.doverennost.filename,
                            content: doc.doverennost.base64,
                            encoding: 'base64'
                        };
                        attach.push(file);
                    }


                }
                if (document.docType == 'Счет на оплату'){
                    var items = req.body.document.items;
                    items.forEach(function (item) {
                        html += '<br/>Номер: '+item.index ;
                        html += '<br/>Единица измерения: '+item.unit ;
                        html += '<br/>Валюта: '+item.currency ;
                        html += '<br/>Код: '+item.code ;
                        html += '<br/>Наименование: '+item.name ;
                        html += '<br/>Количество: '+item.count ;
                        html += '<br/>Сумма: '+item.summ ;
                        html += '<br/>Цена: '+item.price ;
                    });
                    if (doc.naOsnovaniiDogovora){
                        html += '<br/>Основание: номер договора '+ doc.osnovanie.nomerDogovora;
                        html += '<br/>Основание: дата договора'+ doc.osnovanie.date;
                    }
                    html += '<br/> Сформировать счет-фактуру: '+ doc.sdelatShetFacturu;
                    html += '<br/> Сформировать накладную: '+ doc.sdelatNakladnuy;
                }
                if (document.docType == 'Накладная'||document.docType=='Акт выполненных работ'){
                    if (req.body.document.naOsnovanii=="Счет на оплату") {
                        var file = {
                            filename: doc.schetNaOplatu.filename,
                            content: doc.schetNaOplatu.base64,
                            encoding: 'base64'
                        };
                        attach.push(file);
                    }
                    if (req.body.document.naOsnovanii=="Счет-фактура") {
                        var file = {
                            filename: doc.shetFactura.filename,
                            content: doc.shetFactura.base64,
                            encoding: 'base64'
                        };
                        attach.push(file);
                    }

                }

                if (document.docType == 'Поступление'){
                    if (req.body.document.naOsnovanii=="Счет на оплату") {
                        var file = {
                            filename: doc.schetNaOplatu.filename,
                            content: doc.schetNaOplatu.base64,
                            encoding: 'base64'
                        };
                        attach.push(file);
                    }
                    if (req.body.document.naOsnovanii=="Счет-фактура + Накладная") {
                        var file = {
                            filename: doc.shetFactura.filename,
                            content: doc.shetFactura.base64,
                            encoding: 'base64'
                        };
                        attach.push(file);
                        var file2 = {
                            filename: doc.nakladnaya.filename,
                            content: doc.nakladnaya.base64,
                            encoding: 'base64'
                        };
                        attach.push(file2);
                    }
                }

                if (document.docType == 'Платеж'){
                    if (req.body.document.naOsnovanii=="Счет на оплату") {
                        var file = {
                            filename: doc.schetNaOplatu.filename,
                            content: doc.schetNaOplatu.base64,
                            encoding: 'base64'
                        };
                        attach.push(file);
                    }
                    if (req.body.document.naOsnovanii=="Счет-фактура") {
                        var file = {
                            filename: doc.shetFactura.filename,
                            content: doc.shetFactura.base64,
                            encoding: 'base64'
                        };
                        attach.push(file);
                    }

                    html += '<br/> Тип платежа: '+ doc.paymentType;
                }

                if (document.docType == 'Справка'){
                    html += '<br/> ФИО сотрудника '+ doc.nameEmployer;
                    html += '<br/> Должность '+ doc.position;
                    html += '<br/> Место предоставления '+ doc.requiredIn;
                    html += '<br/> Пероид с: '+ doc.dateStart;
                    html += '<br/> Период по: '+ doc.dateFinish;
                    html += '<br/> Требуется справка: '+ doc.helpNeeded;
                    if (doc.helpNeeded='с базы 1с'){
                        html += '<br/> Период справки за последние ';
                        html += '<br/> 3 месяца '+ doc.reference3mounth;
                        html += '<br/> 6 месяца '+ doc.reference6mounth;
                        html += '<br/> 12 месяца '+ doc.reference12mounth;
                    }
                }
                if (document.docType == 'Прием на работу'){
                    html += '<br/> ФИО сотрудника: '+ doc.nameEmployer;
                    html += '<br/> Должность: '+ doc.position;
                    html += '<br/> Место предоставления: '+ doc.requiredIn;
                    html += '<br/> Дата приема на работу: '+ doc.dateStart;

                    var file = {
                        filename: doc.udsLichnosti.filename,
                        content: doc.udsLichnosti.base64,
                        encoding: 'base64'
                    };
                    attach.push(file);

                    var file2 = {
                        filename: doc.pensDogovor.filename,
                        content: doc.pensDogovor.base64,
                        encoding: 'base64'
                    };
                    attach.push(file2);


                    var file3 = {
                        filename: doc.copyDiplom.filename,
                        content: doc.copyDiplom.base64,
                        encoding: 'base64'
                    };
                    attach.push(file3);

                    var file4 = {
                        filename: doc.zayavlenieSotrudnika.filename,
                        content: doc.zayavlenieSotrudnika.base64,
                        encoding: 'base64'
                    };
                    attach.push(file4);



                    if (doc.statusWork =='По совместительству'){
                        var file5 = {
                            filename: doc.spravkaSDrraboti.filename,
                            content: doc.spravkaSDrraboti.base64,
                            encoding: 'base64'
                        };
                        attach.push(file5);
                    }
                }

                if (document.docType == 'Отпуск'){


                    html += '<br/> Вид отпуска '+ doc.helpType;
                    html += '<br/> ФИО сотрудника '+ doc.nameEmployer;
                    html += '<br/> Должность '+ doc.position;
                    html += '<br/> Период с: '+ doc.dateStart;
                    html += '<br/> Период по: '+ doc.dateFinish;
                    var file5 = {
                        filename: doc.zayavlenieSotrudnika.filename,
                        content: doc.zayavlenieSotrudnika.base64,
                        encoding: 'base64'
                    };
                    attach.push(file5);

                    if (doc.helpType == 'По уходу за ребенком'){

                        var file1 = {
                            filename: doc.listVremennoiNetrudosposobnosti.filename,
                            content: doc.listVremennoiNetrudosposobnosti.base64,
                            encoding: 'base64'
                        };
                        attach.push(file1);
                    }
                    if (doc.helpType == 'Отпуск по беременности'){

                        var file1 = {
                            filename: doc.svidetelstvoORogdeniiRebenka.filename,
                            content: doc.svidetelstvoORogdeniiRebenka.base64,
                            encoding: 'base64'
                        };
                        attach.push(file1);
                    }

                }


                if (document.docType == 'Увольнение с работы'){

                    html += '<br/> ФИО сотрудника '+ doc.nameEmployer;
                    html += '<br/> Должность:  '+ doc.position;
                    html += '<br/> Дата увольнения: '+ doc.dateFinish;
                    html += '<br/> Вид расчета: '+ doc.typeOfCalculation;
                    html += '<br/> Причина увольнения:  '+ doc.reasonForLeaving;
                    if (doc.reasonForLeaving == 'По статье'){
                        html += '<br/> Статья: '+ doc.article;
                    }

                    html += '<br/> Посчитать компенсацию: '+ doc.calculateCompensation;

                    var file5 = {
                        filename: doc.zayavlenieSotrudnika.filename,
                        content: doc.zayavlenieSotrudnika.base64,
                        encoding: 'base64'
                    };
                    attach.push(file5);


                }

                if (document.docType == 'Акт сверки'){

                    html += '<br/> Начало периода '+ doc.dateStart;
                    html += '<br/> Конец периода '+ doc.dateFinish;

                }

                if (document.docType == 'Справка об отсутствии задолженности'){

                    html += '<br/> Запросить справку';
                    html += '<br/> С банка '+ doc.referenceToBank;
                    html += '<br/> С НК '+ doc.referenceToHK;
                    html += '<br/> Начало периода '+ doc.dateStart;
                    html += '<br/> Конец периода '+ doc.dateFinish;

                }
                if (document.docType == 'Доверенность'){

                    html += '<br/> Доверенное лицо: '+ doc.confidant;
                    html += '<br/> Должность: '+ doc.position;
                    var file5 = {
                        filename: doc.osnovanie.filename,
                        content: doc.osnovanie.base64,
                        encoding: 'base64'
                    };
                    attach.push(file5);

                }


                html += '<br/> Дополнительная информация: ' + doc.description;
                var transporter = nodemailer.createTransport('smtps://support@mirusdesk.kz:Z123456789z@smtp.gmail.com');

                // setup e-mail data with unicode symbols
                var mailOptions = {
                    from: user.email, // sender address
                    replyTo : user.email,
                    idToEnter : user.idToEnter,
                    to: 'ticket@mirusdesk.kz', // list of receivers
                    // 'ticket@mirusdesk.kz'
                    subject: '"Заявка '+document.docType+' от '+ user.statusCompany + ' '+ user.nameCompany +'{id:'+user.idToEnter+'}' , // Subject line
                    // text: 'Hello world 🐴', // plaintext body
                    html: html, // html body
                    attachments : attach
                };

                // send mail with defined transport object
                transporter.sendMail(mailOptions, function(error, info){
                    if(error){
                        return console.log(error);
                    }
                    console.log('Message sent: ' + info.response);
                });

                done(err, document, 'Document created');
            });


        }
    ], function(err, document, message){
        if(err) return next(err);
        res.status(200).send({ document: document, message: message, status : '200'});
    });
});


app.get('/api/documents/count/all', function (req, res, next) {
    // console.log(req.query.userId);
    Document.count({}, function( err, count){
        if (err) return next(err);
        console.log( "Number of users:", count);
        res.status(200).send({count: count, status : '200', message : 'OK'});
    });
});

app.get('/api/documents/count', function (req, res, next) {
    console.log(req.query.docType);
    console.log(req.query.userId);
    // req.query.userId
    Document.count({docType: req.query.docType, user : req.query.userId }, function( err, count){
        if (err) return next(err);
        console.log( "Count:", count+1);
        res.status(200).send({count: count+1, status : '200', message : 'OK'});
    });
});

app.get('/api/contragents', function(req, res, next){
    console.log(req.query.userId);
    Contragent.find({user : req.query.userId}).exec(function(err, result){
        if (err) return next(err);
        res.send(result);
    });
});

app.get('/api/settings',function (req, res, next) {
    console.log(req.query.userId);
    Settings.findOne({user : req.query.userId}).exec(function(err, result){
        if (err) return next(err);
        res.send(result);
    });
});

app.post('/api/settings', function (req, res, next) {
    async.waterfall([
        function(done) {
            console.log(req.body.userId);
            Settings.findOne({ user: req.body.userId }).exec(function(err, result){
                if (err) return next(err);
                console.log(result);
                var settings = result;
                done(err, settings);

            });

        },
        function(settings, done) {

            if (settings==null) {
                console.log( 'создаем новые настройки');
                var settings = new Settings({
                    copyToEmail : req.body.copyToEmail,
                    sendSMSAfterFihish: req.body.sendSMSAfterFihish,

                    user: req.body.userId
                });
                settings.save(function (err) {
                    done(err, settings, "Settings created");
                });
            }else{
                console.log('Обновляем настройки');
                settings.copyToEmail = req.body.copyToEmail;
                settings.sendSMSAfterFihish = req.body.sendSMSAfterFihish;
                settings.save(function (err) {
                    done(err, settings, "Settings updated");
                });
            }

        }
    ], function(err, settings, message){
        if(err) return next(err);
        res.status(200).send({ settings: settings, message: message, status : '200'});
    });
});


app.get('/api/settings/user',function (req, res, next) {
    User.findById( req.query.userId, 'representative telephone email',  function(err, user) {
        if (err) return next(err);
        console.log(user);
        res.send(user);
    });
});

app.post('/api/settings/user',function (req, res, next) {
    async.waterfall([
        function(done) {
        console.log(req.body.userId);
            User.findById( req.body.userId).exec(function(err, user){
                if (err) return next(err);
                console.log(user);
                done(err, user);

            });

        },
        function(user, done) {

            console.log('Обновляем настройки');
            console.log(req.body);
            user.representative = req.body.representative;
            user.telephone = req.body.telephone;
            user.email = req.body.email;
            console.log(user);
            user.save(function (err) {
                done(err, user, "User updated");
            });


        }
    ], function(err, user, message){
        if(err) return next(err);
        res.status(200).send({ user: user, message: message, status : '200'});
    });
});


// для ангуляра
// app.get('*', function(req, res) {
//     res.redirect('/#' + req.originalUrl);
// });

app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.send(500, { message: err.message });
});

// Start server

var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});
